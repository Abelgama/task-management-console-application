package com.Telerikacademy.oop.Taskmanagement.core.factories;

import com.Telerikacademy.oop.Taskmanagement.commands.*;
import com.Telerikacademy.oop.Taskmanagement.commands.enums.CommandType;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.CommandFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;

public class CommandFactoryImpl implements CommandFactory {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommand(String commandTypeAsString, TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());

        switch (commandType) {
            case CREATETEAM:
                return new CreateTeam(taskManagementFactory, taskManagementRepository);
            case ADDMEMBERTOTEAM:
                return new AddMemberToTeam(taskManagementFactory, taskManagementRepository);
            case CREATEMEMBER:
                return new CreateMember(taskManagementFactory, taskManagementRepository);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembers(taskManagementFactory, taskManagementRepository);
            case CREATEBOARDINTEAM:
                return new CreateBoardinTeam(taskManagementFactory, taskManagementRepository);
            case SHOWALLTEAMBOARDS:
                return new ShowTeamBoards(taskManagementFactory, taskManagementRepository);
            case CREATEBUG:
                return new CreateBug(taskManagementFactory, taskManagementRepository);
            case CREATESTORY:
                return new CreateStory(taskManagementFactory, taskManagementRepository);
            case CREATEFEEDBACK:
                return new CreateFeedback(taskManagementFactory, taskManagementRepository);
            case SHOWTASKSINBOARD:
                return new ShowTasksInBoard(taskManagementFactory, taskManagementRepository);
            case ASSIGNTASKTOMEMBER:
                return new AssignTaskToMember(taskManagementFactory, taskManagementRepository);
            case SHOWMEMBERTASKS:
                return new ShowTasksOfMember(taskManagementFactory, taskManagementRepository);
            case CHANGEBUG:
                return new ChangeBug(taskManagementFactory, taskManagementRepository);
            case CHANGESTORY:
                return new ChangeStory(taskManagementFactory, taskManagementRepository);
            case CHANGEFEEDBACK:
                return new ChangeFeedback(taskManagementFactory, taskManagementRepository);
            case SHOWALLTASKS:
                return new ShowAllTasks(taskManagementFactory, taskManagementRepository);
            case FILTERBYSTATUS:
                return new FilterTasksByStatus(taskManagementFactory, taskManagementRepository);
            case FILTERBYASSIGNEE:
                return new FilterByAssignee(taskManagementFactory, taskManagementRepository);
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }
}
