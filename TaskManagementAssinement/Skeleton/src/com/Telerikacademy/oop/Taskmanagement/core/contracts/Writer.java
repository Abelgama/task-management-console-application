package com.Telerikacademy.oop.Taskmanagement.core.contracts;

public interface Writer {
    void write(String message);

    void writeLine(String message);
}
