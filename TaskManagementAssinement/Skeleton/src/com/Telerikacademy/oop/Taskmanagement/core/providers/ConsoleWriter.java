package com.Telerikacademy.oop.Taskmanagement.core.providers;

import com.Telerikacademy.oop.Taskmanagement.core.contracts.Writer;

public class ConsoleWriter implements Writer {
    public void write(String message) {
        System.out.print(message);
    }

    public void writeLine(String message) {
        System.out.println(message);
    }

}
