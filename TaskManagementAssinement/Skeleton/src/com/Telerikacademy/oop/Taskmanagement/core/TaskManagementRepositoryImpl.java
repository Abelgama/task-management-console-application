package com.Telerikacademy.oop.Taskmanagement.core;

import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.*;

import java.util.HashMap;
import java.util.Map;

public class TaskManagementRepositoryImpl implements TaskManagementRepository {

    private final Map<String, Team> teams;
    private final Map<String, Member> members;
    private final Map<String, Board> boards;
    private final Map<Integer, Task> tasks;
    private final Map<Integer, Bug> bugs;
    private final Map<Integer, Story> stories;
    private final Map<Integer, Feedback> feedbacks;



    public TaskManagementRepositoryImpl() {
        teams = new HashMap<>();
        members = new HashMap<>();
        boards = new HashMap<>();
        tasks = new HashMap<>();
        bugs = new HashMap<>();
        stories = new HashMap<>();
        feedbacks = new HashMap<>();
    }

    public Map<String, Team> getTeams() {
        return teams;
    }

    public Map<String, Member> getMembers() {
        return members;
    }

    public Map<String, Board> getBoards(){
        return boards;
    }

    public Map<Integer, Assignable> getAssignable(){
        Map<Integer, Assignable> map = new HashMap<>();
        map.putAll(bugs);
        map.putAll(stories);

        return map;
    }
    public Map<Integer, Task> getTasks(){
        tasks.putAll(bugs);
        tasks.putAll(stories);
        tasks.putAll(feedbacks);
        return tasks;
    }

    public Map<Integer, Bug> getBugs(){
        return bugs;
    }

    public Map<Integer, Story> getStories(){
        return stories;
    }

    public Map<Integer, Feedback> getFeedbacks(){
        return feedbacks;
    }

}
