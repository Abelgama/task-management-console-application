package com.Telerikacademy.oop.Taskmanagement.core.contracts;

public interface Reader {
    String readLine();
}
