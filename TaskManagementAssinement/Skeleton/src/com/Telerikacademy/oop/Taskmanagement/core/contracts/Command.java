package com.Telerikacademy.oop.Taskmanagement.core.contracts;

import java.util.List;

public interface Command {

    String execute(List<String> parameters);
    String getResult();
}
