package com.Telerikacademy.oop.Taskmanagement.core.contracts;

import com.Telerikacademy.oop.Taskmanagement.models.contracts.*;

import java.util.Map;

public interface TaskManagementRepository {

    Map<String, Team> getTeams();
    Map<String, Member> getMembers();
    Map<String, Board> getBoards();
    Map<Integer, Task> getTasks();
    Map<Integer, Bug> getBugs();
    Map<Integer, Story> getStories();
    Map<Integer, Feedback> getFeedbacks();
    Map<Integer, Assignable> getAssignable();

    }


