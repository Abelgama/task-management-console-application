package com.Telerikacademy.oop.Taskmanagement.core.contracts;

import com.Telerikacademy.oop.Taskmanagement.models.common.enums.*;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.*;


public interface TaskManagementFactory {

    Team createTeam(String name);

    Comment createComment(String content, String author);

    Member createMember(String name);

    Board createBoard(String name);

    Bug createBug(String title, String description, BugStatus bugStatus, Priority priority, TaskSeverity severity);

    Story createStory(String title, String description, StoryStatus storyStatus, Priority priority, Size size);

    Feedback createFeedback(String title, String description, FeedbackStatus feedbackStatus, int rating);

}
