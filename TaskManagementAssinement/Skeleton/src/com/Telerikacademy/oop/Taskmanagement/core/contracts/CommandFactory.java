package com.Telerikacademy.oop.Taskmanagement.core.contracts;

public interface CommandFactory {
    Command createCommand(String commandTypeAsString, TaskManagementFactory factory, TaskManagementRepository taskManagementRepository);

}
