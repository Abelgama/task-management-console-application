package com.Telerikacademy.oop.Taskmanagement.core;

import com.Telerikacademy.oop.Taskmanagement.core.contracts.*;
import com.Telerikacademy.oop.Taskmanagement.core.factories.CommandFactoryImpl;
import com.Telerikacademy.oop.Taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.Telerikacademy.oop.Taskmanagement.core.providers.CommandParserImpl;
import com.Telerikacademy.oop.Taskmanagement.core.providers.ConsoleReader;
import com.Telerikacademy.oop.Taskmanagement.core.providers.ConsoleWriter;

import java.util.List;

public class TaskManagementEngine {

    private static final String TERMINATION_COMMAND = "Exit";

    private final TaskManagementFactory taskManagementFactory;
    private final CommandParser commandParser;
    private final CommandFactory commandFactory;
    private final Reader reader;
    private final Writer writer;
    private final TaskManagementRepository taskManagementRepository;

    public TaskManagementEngine() {
        taskManagementFactory = new TaskManagementFactoryImpl();
        commandParser = new CommandParserImpl();
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        commandFactory = new CommandFactoryImpl();
        taskManagementRepository = new TaskManagementRepositoryImpl();
    }

    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);

            } catch (Exception ex) {
                writer.writeLine(ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }

        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandName, taskManagementFactory, taskManagementRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
