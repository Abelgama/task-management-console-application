package com.Telerikacademy.oop.Taskmanagement.core.factories;

import com.Telerikacademy.oop.Taskmanagement.core.contracts.CommandFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.models.BoardImpl;
import com.Telerikacademy.oop.Taskmanagement.models.MembersImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.BugImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.CommentImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.FeedbackImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.StoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.TeamImpl;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.*;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.*;

import java.util.List;

public class TaskManagementFactoryImpl implements TaskManagementFactory {

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Comment createComment(String content, String author) {

        return new CommentImpl(content, author);
    }
    @Override
    public Member createMember(String name) {

        return new MembersImpl(name);
    }
    @Override
    public Board createBoard(String name) {

        return new BoardImpl(name);
    }

    @Override
    public Bug createBug(String title, String description, BugStatus bugStatus, Priority priority, TaskSeverity taskSeverity) {

        return new BugImpl(title,description,bugStatus,priority,taskSeverity);
    }

    @Override
    public Story createStory(String title, String description, StoryStatus storyStatus, Priority priority, Size size) {

        return new StoryImpl(title,description,storyStatus,priority,size);
    }

    @Override
    public Feedback createFeedback(String title, String description, FeedbackStatus feedbackStatus, int rating) {

        return new FeedbackImpl(title,description,feedbackStatus,rating);
    }
}



