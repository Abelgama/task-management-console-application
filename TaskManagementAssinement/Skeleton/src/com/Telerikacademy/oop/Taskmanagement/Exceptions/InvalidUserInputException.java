package com.Telerikacademy.oop.Taskmanagement.Exceptions;

public class InvalidUserInputException extends RuntimeException{
    public InvalidUserInputException(String message) {
        super(message);
    }
}
