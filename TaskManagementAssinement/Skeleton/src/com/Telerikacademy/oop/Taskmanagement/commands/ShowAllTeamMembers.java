package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Team;

import java.util.List;

public class ShowAllTeamMembers implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public ShowAllTeamMembers(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
        this.taskManagementFactory = taskManagementFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        String teamToShow = parameters.get(0);
        result = showTeamMembers(teamToShow);
        return result;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String showTeamMembers(String teamToShow) {
        if (!taskManagementRepository.getTeams().containsKey(teamToShow)) {
            return String.format(CommandConstants.TEAM_DOES_NOT_EXIST, teamToShow);
        }

        Team team = taskManagementRepository.getTeams().get(teamToShow);

        return team.printMembers();
    }
}
