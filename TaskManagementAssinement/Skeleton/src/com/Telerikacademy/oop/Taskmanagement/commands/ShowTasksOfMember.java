package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;

import java.util.List;

public class ShowTasksOfMember implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;

    public ShowTasksOfMember(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
        this.taskManagementFactory = taskManagementFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        String personToShow = parameters.get(0);
        result = showMemberTasks(personToShow);
        return result ;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String showMemberTasks(String personToShow) {
        if (!taskManagementRepository.getMembers().containsKey(personToShow)) {
            return String.format(CommandConstants.MEMBER_DOES_NOT_EXIST, personToShow);
        }

        Member member = taskManagementRepository.getMembers().get(personToShow);

        return member.printTasks();
    }
}
