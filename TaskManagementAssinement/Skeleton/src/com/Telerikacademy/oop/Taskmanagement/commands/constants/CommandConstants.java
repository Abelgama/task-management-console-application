package com.Telerikacademy.oop.Taskmanagement.commands.constants;

public class CommandConstants {

    public static final String INVALID_COMMAND = "Invalid command! %s";

    public static final String MEMBER_CREATED = "User %s created!";
    public static final String COMMENT_ADDED_SUCCESSFULLY = "User %s added comment successfully!";
    public static final String TASK_DOES_NOT_EXISTS = "Task with ID %d does not exist!";
    public static final String TEAM_CREATED = "Team %s was created!";
    public static final String TEAM_EXISTS = "A team by the name of %s already exists!";
    public static final String MEMBER_ADDED_TO_TEAM = "Member %s successfully added to team %s !";
    public static final String MEMBER_DOES_NOT_EXIST = "Member by the name of %s does not exist!";
    public static final String TEAM_DOES_NOT_EXIST = "Team with name %s does not exists!";
    public static final String MEMBER_EXISTS = "Member %s already exists!";

    public static final String BOARD_EXIST = "A board by the name of %s already exists!";;
    public static final String BOARD_CREATED = "Board %s was created!";

    public static final String BOARD_DOES_NOT_EXIST = "Board by the name of %s does not exist!";
    public static final String TASK_CREATED = "Task with ID %d and title %s was created!";


    public static final String MEMBER_DOES_NOT_EXIST_IN_TEAM = "The person %s is not a member of team %s!";
    public static final String TASK_DOES_NOT_EXIST_IN_TEAM_BOARD = "Task with ID %d does not exists in team boards";
    public static final String TASK_ASSIGNED_TO_MEMBER ="Task with ID %d was assigned to member %s";
    public static final String BUG_MODIFIED ="%s of bug with ID %d was modified to %s";
    public static final String STORY_MODIFIED ="%s of story with ID %d was modified to %s";
    public static final String FEEDBACK_MODIFIED ="%s of feedback with ID %d was modified to %s";

}
