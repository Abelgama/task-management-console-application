package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.FeedbackStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Size;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.StoryStatus;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Feedback;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Story;

import java.util.List;

public class CreateFeedback implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public CreateFeedback(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String boardToAdd = parameters.get(0);
        String title = parameters.get(1);
        String description = parameters.get(2);
        FeedbackStatus feedbackStatus =FeedbackStatus.valueOf(parameters.get(3).toUpperCase());
        int rating = Integer.parseInt(parameters.get(4));
        result = createFeedbackInBoard(boardToAdd,title,description,feedbackStatus, rating);
        return result ;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createFeedbackInBoard(String boardToAdd, String title, String description, FeedbackStatus feedbackStatus, int rating) {

        if (!taskManagementRepository.getBoards().containsKey(boardToAdd)) {
            return String.format(CommandConstants.BOARD_DOES_NOT_EXIST, boardToAdd);
        }
        Board board = taskManagementRepository.getBoards().get(boardToAdd);

        Feedback feedback = taskManagementFactory.createFeedback(title,description,feedbackStatus, rating);

        taskManagementRepository.getFeedbacks().put(feedback.getID(),feedback);

        board.addTask(feedback);

        return String.format(CommandConstants.TASK_CREATED, feedback.getID(),feedback.getTitle());
    }
}
