package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Team;

import java.util.List;

public class CreateBoardinTeam implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public CreateBoardinTeam(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String teamName = parameters.get(0);
        String boardToAdd = parameters.get(1);
        result = createBoard(teamName,boardToAdd);
        return result ;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createBoard(String teamName , String boardToAdd) {
        if (!taskManagementRepository.getTeams().containsKey(teamName)) {
            return String.format(CommandConstants.TEAM_DOES_NOT_EXIST, teamName);
        }
        Team team = taskManagementRepository.getTeams().get(teamName );

        if (taskManagementRepository.getBoards().containsKey(boardToAdd)) {
            return String.format(CommandConstants.BOARD_EXIST, boardToAdd);
        }
        Board board = taskManagementFactory.createBoard(boardToAdd);


        taskManagementRepository.getBoards().put(boardToAdd, board);

        team.addBoard(board);

        //team.getActivity().add("member pesho was added to team")
        return String.format(CommandConstants.BOARD_CREATED, boardToAdd);
    }
}
