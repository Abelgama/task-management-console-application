package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Team;

import java.util.List;

public class ShowTasksInBoard implements Command {

    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;

    public ShowTasksInBoard(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
        this.taskManagementFactory = taskManagementFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        String boardToShow = parameters.get(0);
        result = showBoardTasks(boardToShow);
        return result;

    }
    @Override
    public String getResult() {
        return result;
    }

    private String showBoardTasks(String boardToShow) {
        if (!taskManagementRepository.getBoards().containsKey(boardToShow)) {
            return String.format(CommandConstants.BOARD_DOES_NOT_EXIST, boardToShow);
        }

        Board board = taskManagementRepository.getBoards().get(boardToShow);

        return board.printTasks();
    }
}
