package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Team;

import java.util.List;

public class AddMemberToTeam implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public AddMemberToTeam(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String teamNameToAdd = parameters.get(0);
        String memberToAdd = parameters.get(1);
        result = addToTeam(teamNameToAdd, memberToAdd);
        return result;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String addToTeam(String teamNameToAdd, String memberToAdd) {
        if (!taskManagementRepository.getTeams().containsKey(teamNameToAdd)) {
            return String.format(CommandConstants.TEAM_DOES_NOT_EXIST, teamNameToAdd);
        }

        if (!taskManagementRepository.getMembers().containsKey(memberToAdd)) {
            return String.format(CommandConstants.MEMBER_DOES_NOT_EXIST, memberToAdd);
        }

        Team team = taskManagementRepository.getTeams().get(teamNameToAdd);
        Member member = taskManagementRepository.getMembers().get(memberToAdd);

        team.addMember(member);

        return String.format(CommandConstants.MEMBER_ADDED_TO_TEAM, memberToAdd, teamNameToAdd);
    }
}
