package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;

import java.util.List;
import java.util.stream.Collectors;

public class FilterByAssignee implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;

    public FilterByAssignee(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String assigneeToFilterBy = parameters.get(0);
        result = filterTasksByAssignee(assigneeToFilterBy);

        return result;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String filterTasksByAssignee(String assigneeToFilterBy) {
        System.out.println(taskManagementRepository.getTasks());
        final int[] count ={1};
        StringBuilder string = new StringBuilder();
        string.append("****FILTERED BY ASSIGNEE****").append(System.lineSeparator());
        taskManagementRepository.getAssignable().entrySet()
                .stream()
                .filter(value -> value.getValue().getAssignee().getName().equals(assigneeToFilterBy))
                .forEach(value-> string.append(String.format("%d. Task ID: %d%n Title: %s%n",count[0]++,value.getValue().getID(),
                        value.getValue().getTitle())));

        return string.toString();
    }
}
