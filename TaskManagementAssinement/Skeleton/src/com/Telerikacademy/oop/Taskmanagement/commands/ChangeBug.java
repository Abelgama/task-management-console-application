package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.BugStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.TaskSeverity;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;

import javax.print.attribute.standard.Severity;
import java.util.List;
import java.util.Locale;

public class ChangeBug implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public ChangeBug(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String parameter = parameters.get(0);
        String type = parameters.get(1);
        int taskID = Integer.parseInt(parameters.get(2));
        result = changeBugParameters(parameter,type,taskID);
        return  result;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String changeBugParameters(String parameter,String type,int taskID) {

        if (!taskManagementRepository.getBugs().containsKey(taskID)) {
            return String.format(CommandConstants.TASK_DOES_NOT_EXISTS, taskID);
        }

        Bug bug = taskManagementRepository.getBugs().get(taskID);

        if (parameter.toLowerCase().equals("priority")){
            Priority newPriority = Priority.valueOf(type.toUpperCase());
            bug.modifyPriority(newPriority);

        }else if (parameter.toLowerCase().equals("severity")){
            TaskSeverity newSeverity = TaskSeverity.valueOf(type.toUpperCase());
            bug.modifySeverity(newSeverity);

        }else if (parameter.toLowerCase().equals("status")){
            BugStatus newStatus = BugStatus.valueOf(type.toUpperCase());
            bug.modifyStatus(newStatus);
        }


        return String.format(CommandConstants.BUG_MODIFIED, parameter,taskID,type);
    }
}
