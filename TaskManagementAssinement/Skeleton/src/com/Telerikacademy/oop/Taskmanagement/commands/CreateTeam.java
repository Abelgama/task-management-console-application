package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Team;

import java.util.List;

public class CreateTeam implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public CreateTeam(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String teamName = parameters.get(0);
        result = createTeam(teamName);

        return result;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createTeam(String teamName) {
        if (taskManagementRepository.getTeams().containsKey(teamName)) {
            return String.format(CommandConstants.TEAM_EXISTS, teamName);
        }

        Team team = taskManagementFactory.createTeam(teamName);
        taskManagementRepository.getTeams().put(teamName, team);

        return String.format(CommandConstants.TEAM_CREATED, teamName);
    }
}

