package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Team;

import java.util.List;

public class CreateMember implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public CreateMember(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String memberName = parameters.get(0);
        result = createMember(memberName);
        return result;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createMember(String memberName) {
        if (taskManagementRepository.getMembers().containsKey(memberName)) {
            return String.format(CommandConstants.MEMBER_EXISTS, memberName);
        }

        Member member = taskManagementFactory.createMember(memberName);
        taskManagementRepository.getMembers().put(memberName, member);

        return String.format(CommandConstants.MEMBER_CREATED, memberName);
    }
}
