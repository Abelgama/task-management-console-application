package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.*;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Story;

import java.util.List;

public class CreateStory implements Command {

    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public CreateStory(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String boardToAdd = parameters.get(0);
        String title = parameters.get(1);
        String description = parameters.get(2);
        StoryStatus storyStatus =StoryStatus.valueOf(parameters.get(3).toUpperCase());
        Priority priority =Priority.valueOf(parameters.get(4).toUpperCase());
        Size size = Size.valueOf(parameters.get(5).toUpperCase());
        result = createStoryInBoard(boardToAdd,title,description,storyStatus,priority, size);
        return result;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createStoryInBoard(String boardToAdd, String title, String description, StoryStatus storyStatus, Priority priority, Size size) {

        if (!taskManagementRepository.getBoards().containsKey(boardToAdd)) {
            return String.format(CommandConstants.BOARD_DOES_NOT_EXIST, boardToAdd);
        }
        Board board = taskManagementRepository.getBoards().get(boardToAdd);

       Story story = taskManagementFactory.createStory(title,description, storyStatus,priority, size);

        taskManagementRepository.getStories().put(story.getID(),story);

        board.addTask(story);

        return String.format(CommandConstants.TASK_CREATED, story.getID(),story.getTitle());
    }
}

