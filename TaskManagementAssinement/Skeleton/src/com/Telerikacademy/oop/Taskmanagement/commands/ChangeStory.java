package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.*;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Story;

import java.util.List;

public class ChangeStory implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public ChangeStory(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String parameter = parameters.get(0);
        String type = parameters.get(1);
        int taskID = Integer.parseInt(parameters.get(2));
        result = changeStoryParameters(parameter,type,taskID);
        return result ;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String changeStoryParameters(String parameter,String type,int taskID) {

        if (!taskManagementRepository.getStories().containsKey(taskID)) {
            return String.format(CommandConstants.TASK_DOES_NOT_EXISTS, taskID);
        }

        Story story = taskManagementRepository.getStories().get(taskID);

        if (parameter.equalsIgnoreCase("priority")){
            Priority newPriority = Priority.valueOf(type.toUpperCase());
            story.modifyPriority(newPriority);

        }else if (parameter.equalsIgnoreCase("size")){
            Size newSize = Size.valueOf(type.toUpperCase());
            story.modifySize(newSize);

        }else if (parameter.equals("status")){
            StoryStatus newStatus = StoryStatus.valueOf(type.toUpperCase());
            story.modifyStatus(newStatus);
        }


        return String.format(CommandConstants.STORY_MODIFIED, parameter,taskID,type);
    }
}
