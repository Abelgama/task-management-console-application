package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.BugStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.TaskSeverity;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;

import java.util.List;

public class ShowAllTasks implements Command {

    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public ShowAllTasks(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String typeOfTask = parameters.get(0);
        result = showTasks(typeOfTask);
        return result;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String showTasks(String typeOfTask) {

       final int[] count = {1};
        StringBuilder string = new StringBuilder();
        if(typeOfTask.equalsIgnoreCase("all")){
            string.append("****TASKS****").append(System.lineSeparator());
            taskManagementRepository.getTasks().forEach((key,value)->string.append(
                    String.format("%d. Task ID: %d%n Title: %s",count[0]++,value.getID(),value.getTitle()))
                    .append(System.lineSeparator()));

        }else if (typeOfTask.equalsIgnoreCase("bug")){
            string.append("****BUGS****").append(System.lineSeparator());
            taskManagementRepository.getBugs().forEach((key,value)->string.append(
                    String.format("%d. Task ID: %d%n Title: %s",count[0]++,value.getID(),value.getTitle()))
                    .append(System.lineSeparator()));

        }else if (typeOfTask.equalsIgnoreCase("story")){
            string.append("****STORIES****").append(System.lineSeparator());
            taskManagementRepository.getStories().forEach((key,value)->string.append(
                    String.format("%d. Task ID: %d%n Title: %s",count[0]++,value.getID(),value.getTitle()))
                    .append(System.lineSeparator()));

        }else if (typeOfTask.equalsIgnoreCase("feedback")){
            string.append("****FEEDBACKS****").append(System.lineSeparator());
            taskManagementRepository.getFeedbacks().forEach((key,value)->string.append(
                    String.format("%d. Task ID: %d%n Title: %s",count[0]++,value.getID(),value.getTitle()))
                    .append(System.lineSeparator()));

        }
        return string.toString();
    }
}
