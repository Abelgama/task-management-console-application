package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.FeedbackStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Size;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.StoryStatus;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Feedback;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Story;

import java.util.List;

public class ChangeFeedback implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public ChangeFeedback(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String parameter = parameters.get(0);
        String type = parameters.get(1);
        int taskID = Integer.parseInt(parameters.get(2));
        result = changeBugParameters(parameter,type,taskID);
        return result ;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String changeBugParameters(String parameter,String type,int taskID) {

        if (!taskManagementRepository.getFeedbacks().containsKey(taskID)) {
            return String.format(CommandConstants.TASK_DOES_NOT_EXISTS, taskID);
        }

        Feedback feedback = taskManagementRepository.getFeedbacks().get(taskID);

        if (parameter.equalsIgnoreCase("priority")){
            int newRating = Integer.parseInt(type);
            feedback.modifyRating(newRating);

        }else if (parameter.equals("status")){
            FeedbackStatus newStatus = FeedbackStatus.valueOf(type.toUpperCase());
            feedback.modifyStatus(newStatus);
        }


        return String.format(CommandConstants.FEEDBACK_MODIFIED, parameter,taskID,type);
    }
}
