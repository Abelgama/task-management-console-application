package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.*;

import java.util.List;

public class AssignTaskToMember implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public AssignTaskToMember(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String teamNameOfMember = parameters.get(0);
        String memberName = parameters.get(1);
        int taskID = Integer.parseInt(parameters.get(2));
        result = addTaskToMember(teamNameOfMember, memberName, taskID);
        return result ;
    }

    @Override
    public String getResult() {
        return result;
    }

    //ASSIGNTASKTOMEMBER team1 pesho 34
    private String addTaskToMember(String teamNameOfMember, String memberName, int taskID) {
        if (!taskManagementRepository.getTeams().containsKey(teamNameOfMember)) {
            return String.format(CommandConstants.TEAM_DOES_NOT_EXIST, teamNameOfMember);
        }

        if (!taskManagementRepository.getMembers().containsKey(memberName)) {
            return String.format(CommandConstants.MEMBER_DOES_NOT_EXIST, memberName);
        }

        Team team = taskManagementRepository.getTeams().get(teamNameOfMember);
        Member member = taskManagementRepository.getMembers().get(memberName);
        Task task = taskManagementRepository.getTasks().get(taskID);

        taskManagementRepository.getMembers().forEach((key, value) -> {
            if (value.getTasks().contains(task)){
                value.removeTask(task);
            }
        });

        if (!team.getTeamMembers().contains(member)) {
            return String.format(CommandConstants.MEMBER_DOES_NOT_EXIST_IN_TEAM, memberName, teamNameOfMember);
        }
        team.getTeamBoards().forEach((value) -> {
            if (value.getBoardTasks().contains(task)) {
                member.addTask(task);
            }
        });

        if (!member.getTasks().contains(task)) {
            return String.format(CommandConstants.TASK_DOES_NOT_EXIST_IN_TEAM_BOARD, taskID);
        }

        if (taskManagementRepository.getBugs().containsKey(taskID)) {
            Bug bug = taskManagementRepository.getBugs().get(taskID);
            bug.modifyAssignee(member);


        } else if (taskManagementRepository.getStories().containsKey(taskID)) {
            Story story = taskManagementRepository.getStories().get(taskID);
            story.modifyAssignee(member);
        }
        return String.format(CommandConstants.TASK_ASSIGNED_TO_MEMBER, taskID, memberName);
    }
}
