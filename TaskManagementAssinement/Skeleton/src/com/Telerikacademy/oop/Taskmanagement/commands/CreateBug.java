package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.BugStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.TaskSeverity;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;

import java.util.List;


public class CreateBug implements Command {

    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public CreateBug(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String boardToAdd = parameters.get(0);
        String title = parameters.get(1);
        String description = parameters.get(2);
        BugStatus bugStatus =BugStatus.valueOf(parameters.get(3).toUpperCase());
        Priority priority =Priority.valueOf(parameters.get(4).toUpperCase());
        TaskSeverity taskSeverity = TaskSeverity.valueOf(parameters.get(5).toUpperCase());
        result = createBugInBoard(boardToAdd,title,description,bugStatus,priority, taskSeverity);
        return result ;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createBugInBoard(String boardToAdd, String title, String description, BugStatus bugStatus, Priority priority, TaskSeverity taskSeverity) {

        if (!taskManagementRepository.getBoards().containsKey(boardToAdd)) {
            return String.format(CommandConstants.BOARD_DOES_NOT_EXIST, boardToAdd);
        }
        Board board = taskManagementRepository.getBoards().get(boardToAdd);

        Bug bug = taskManagementFactory.createBug(title,description,bugStatus,priority, taskSeverity);

        taskManagementRepository.getBugs().put(bug.getID(),bug);

        board.addTask(bug);

        return String.format(CommandConstants.TASK_CREATED, bug.getID(),bug.getTitle());
    }
}
