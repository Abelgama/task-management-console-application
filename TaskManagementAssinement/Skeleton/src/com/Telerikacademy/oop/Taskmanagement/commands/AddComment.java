package com.Telerikacademy.oop.Taskmanagement.commands;

import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.Command;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Comment;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Task;

import java.util.List;

public class AddComment implements Command {
    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;
    private String result;


    public AddComment(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String content = parameters.get(0);
        String author = parameters.get(1);
        int taskID = Integer.parseInt(parameters.get(2));
        result = addCommentToTask(content, author,taskID);
        return result;
    }

    @Override
    public String getResult() {
        return result;
    }

    private String addCommentToTask(String content, String author,int taskID) {
        //Member member = taskManagementRepository.getMembers().get(author);
        if (!taskManagementRepository.getTasks().containsKey(taskID)) {
            return String.format(CommandConstants.TASK_DOES_NOT_EXISTS, taskID);
        }
        if (!taskManagementRepository.getMembers().containsKey(author)) {
            return String.format(CommandConstants.MEMBER_DOES_NOT_EXIST, author);
        }
        Task taskToAddComment = taskManagementRepository.getTasks().get(taskID);

        Member member = taskManagementRepository.getMembers().get(author);

        Comment comment = taskManagementFactory.createComment(content,author);


        member.addComment(comment, taskToAddComment);

        return String.format(CommandConstants.COMMENT_ADDED_SUCCESSFULLY,member.getName());
    }
}
