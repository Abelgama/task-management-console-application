package com.Telerikacademy.oop.Taskmanagement.models;

import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {
    private static final int NAME_MIN_LENGTH = 2;
    private static final int NAME_MAX_LENGTH = 15;

    private String name;
    private List<Member> members;
    private List<Board> boards;
    private List<String> activity;

    public TeamImpl(String name){
        setName(name);
        members = new ArrayList<>();
        boards = new ArrayList<>();
        activity = new ArrayList<>();

    }


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Member> getTeamMembers() {
        return members ;
    }

    @Override
    public List<Board> getTeamBoards() {
        return boards ;
    }

    @Override
    public void addMember(Member member) {
        members.add(member);

    }

    @Override
    public void addBoard(Board board) {
      boards.add(board);
    }

    @Override
    public String printMembers() {

        if (members.size() == 0) {
            return String.format("#Team: %s%n" +
                    " #No members in this team", name);
        }
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("#Team: %s%n", name));
        for (Member member : members) {
            strBuilder.append(member.print());
           // strBuilder.append(" ===");
            //strBuilder.append(System.getProperty("line.separator"));
        }
        return strBuilder.toString();
    }

    @Override
    public String printBoards() {

        if (boards.size() == 0) {
            return String.format("#Team: %s%n" +
                    " #No boards in this team", name);
        }
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("#Team: %s%n", name));
        for (Board board : boards) {
            strBuilder.append(board.print());
        }
        return strBuilder.toString();
    }
}

