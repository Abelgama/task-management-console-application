package com.Telerikacademy.oop.Taskmanagement.models;

import com.Telerikacademy.oop.Taskmanagement.models.common.Validator;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Comment;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MembersImpl implements Member {

    private static int uniqueID = 0;
    private  int memberID ;
    private String name;
    private List<Task> tasks;
    private List<String> activityHistory;

    public MembersImpl(String name) {
        setMemberID(memberID);
        setName(name);
        tasks = new ArrayList<>();
        activityHistory = new ArrayList<>();

    }

    //SETTERS

    public void setMemberID(int taskID) {

        this.memberID =++uniqueID ;
    }

    public void setName(String name) {
        this.name = name;
    }

    // GETTERS

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    //OTHER
    @Override
    public void addComment(Comment commentToAdd, Task taskToAddComment) {
        Validator.validateArgumentIsNotNull(commentToAdd, "");
        Validator.validateArgumentIsNotNull(taskToAddComment, "");
        taskToAddComment.getComments().add(commentToAdd);
    }

    @Override
    public void addTask(Task task) {
        tasks.add(task);
    }

    @Override
    public void removeTask(Task task) {
        tasks.remove(task);

    }

    @Override
    public String print() {
        return String.format("-%s %n", getName());
    }

    @Override
    public String printTasks() {

        if (tasks.size() == 0) {
            return String.format("#Member Tasks: %s%n" +
                    " #No members in this team", name);
        }
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("#Member Tasks: %s%n", name));
        for (Task task : tasks) {
            strBuilder.append(task.print());
            strBuilder.append(String.format(" --- %n"));
            //strBuilder.append(System.getProperty("line.separator"));
        }
        strBuilder.append(" ===");
        return strBuilder.toString();
    }
}
