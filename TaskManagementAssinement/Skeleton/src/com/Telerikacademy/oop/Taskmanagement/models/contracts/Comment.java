package com.Telerikacademy.oop.Taskmanagement.models.contracts;

public interface Comment {

    String getAuthor();

    String getContent();
}
