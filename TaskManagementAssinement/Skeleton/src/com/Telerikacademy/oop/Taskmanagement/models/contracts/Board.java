package com.Telerikacademy.oop.Taskmanagement.models.contracts;

import java.util.List;
import java.util.Map;

public interface Board {

    String getName();

    List<String> getActivityHistory();

    List<Task> getBoardTasks();

    void addTask(Task taskToAdd);

    String print();

    String printTasks();


}
