package com.Telerikacademy.oop.Taskmanagement.models.Task;

import com.Telerikacademy.oop.Taskmanagement.models.common.enums.*;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Story;

public class StoryImpl extends AssignableTaskImpl implements Story {

    private Size size;
    private StoryStatus status;

    public StoryImpl(String title, String description, StoryStatus status,Priority priority,Size size) {
        super(title, description, priority);
        setSize(size);
        setStatus(status);
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public void setStatus(StoryStatus status) {
        this.status = status;
    }

    @Override
    public  void modifyPriority(Priority priority){
        setPriority(priority);
    }

    @Override
    public  void modifyStatus(StoryStatus status){

        setStatus(status);
    }

    @Override
    public  void modifySize(Size size){

        setSize(size);
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public StoryStatus getStatus() {
        return status;
    }

    @Override
    public void modifyAssignee(Member assignee) {
        setAssignee(assignee);
    }
}
