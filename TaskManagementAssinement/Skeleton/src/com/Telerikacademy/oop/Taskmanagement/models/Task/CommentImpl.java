package com.Telerikacademy.oop.Taskmanagement.models.Task;

import com.Telerikacademy.oop.Taskmanagement.models.common.Validator;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Comment;

import static com.Telerikacademy.oop.Taskmanagement.models.common.Constants.*;

public class CommentImpl implements Comment {

    private String author;
    private String content;

    public CommentImpl(String author,String content) {
        setAuthor(author);
        setContent(content);
    }

    @Override
    public String getAuthor() {
        return author;
    }

    private void setAuthor(String author) {
        Validator.validateArgumentIsNotNull(author, AUTHOR_NOT_NULL_MESSAGE);
        this.author = author;
    }

    @Override
    public String getContent() {
        return content;
    }

    private void setContent(String content) {
        Validator.validateArgumentIsNotNull(content, CONTENT_NOT_NULL_MESSAGE);
        Validator.validateIntRange(content.length(), CONTENT_LEN_MIN, CONTENT_LEN_MAX,CONTENT_ERROR_MESSAGE);
        this.content = content;
    }

}
