package com.Telerikacademy.oop.Taskmanagement.models.contracts;

import com.Telerikacademy.oop.Taskmanagement.models.common.enums.FeedbackStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Size;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.StoryStatus;

public interface Feedback extends Task {

    int getRating();

    FeedbackStatus getStatus();

    void modifyStatus(FeedbackStatus status);

    void modifyRating(int rating);

}
