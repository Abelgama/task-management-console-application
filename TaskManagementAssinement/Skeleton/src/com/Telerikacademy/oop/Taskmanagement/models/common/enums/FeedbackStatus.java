package com.Telerikacademy.oop.Taskmanagement.models.common.enums;

import com.Telerikacademy.oop.Taskmanagement.models.contracts.StatusBase;

public enum FeedbackStatus implements StatusBase {

    DONE,
    NEW,
    SCHEDULED,
    UNSCHEDULED;

    @Override
    public String toString() {
        switch (this) {
            case DONE:
                return "Done";
            case NEW:
                return "New";
            case SCHEDULED:
                return "Scheduled";
            case UNSCHEDULED:
                return "Unscheduled";
            default:
                return "";
        }
    }
}
