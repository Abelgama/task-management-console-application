package com.Telerikacademy.oop.Taskmanagement.models.common.enums;

public enum TaskSeverity {
    CRITICAL,
    MAJOR,
    MINOR;

    @Override
    public String toString() {
        switch (this) {
            case CRITICAL:
                return "Critical";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            default:
                return "";
        }
    }
}
