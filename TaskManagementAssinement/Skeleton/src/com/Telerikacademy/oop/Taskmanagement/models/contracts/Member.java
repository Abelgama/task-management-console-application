package com.Telerikacademy.oop.Taskmanagement.models.contracts;

import java.util.List;
import java.util.Map;

public interface Member {

    String getName();

    List<Task> getTasks();

    void addTask(Task task);

    void removeTask(Task task);

    void addComment(Comment commentToAdd, Task taskToAddComment);

    String print();

    String printTasks();

}
