package com.Telerikacademy.oop.Taskmanagement.models.Task;

import com.Telerikacademy.oop.Taskmanagement.models.common.enums.BugStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.TaskSeverity;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;

public class BugImpl extends AssignableTaskImpl implements Bug {

private TaskSeverity severity;
private BugStatus status;

    public BugImpl( String title, String description, BugStatus bugStatus, Priority priority, TaskSeverity severity) {
        super(title, description, priority);
        setSeverity(severity);
        setStatus(bugStatus);
    }

    public void setSeverity(TaskSeverity severity) {
        this.severity = severity;
    }

    public void setStatus(BugStatus status) {
        this.status = status;
    }

    @Override
    public  void modifyAssignee(Member assignee){

        setAssignee(assignee);
    }

    @Override
    public  void modifyPriority(Priority priority){

        setPriority(priority);
    }

    @Override
    public  void modifyStatus(BugStatus status){

        setStatus(status);
    }

    @Override
    public  void modifySeverity(TaskSeverity severity){

        setSeverity(severity);
    }

    @Override
    public TaskSeverity getSeverity() {
        return severity;
    }

    @Override
    public BugStatus getStatus() {
        return status;
    }
}
