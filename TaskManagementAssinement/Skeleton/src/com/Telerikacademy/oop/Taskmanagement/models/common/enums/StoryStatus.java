package com.Telerikacademy.oop.Taskmanagement.models.common.enums;

import com.Telerikacademy.oop.Taskmanagement.models.contracts.StatusBase;

public enum StoryStatus implements StatusBase {
    NOTDONE,
    INPROGRESS,
    DONE;

    @Override
    public String toString() {
        switch (this) {
            case NOTDONE:
                return "NotDone";
            case INPROGRESS:
                return "InProgress";
            case DONE:
                return "Done";
            default:
                return "";
        }
    }
}
