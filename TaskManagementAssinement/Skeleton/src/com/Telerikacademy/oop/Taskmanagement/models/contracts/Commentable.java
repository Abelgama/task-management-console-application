package com.Telerikacademy.oop.Taskmanagement.models.contracts;

import java.util.List;

public interface Commentable {
    List<Comment> getComments();
}
