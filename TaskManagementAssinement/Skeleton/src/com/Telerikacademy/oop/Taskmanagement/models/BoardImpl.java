package com.Telerikacademy.oop.Taskmanagement.models;

import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardImpl implements Board {
    private String name;
    private List<Task> tasks;
    private List<String> activityHistory;


    public BoardImpl(String name) {
        setName(name);
        tasks = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<String> getActivityHistory() {
        return null;
    }

    @Override
    public List<Task> getBoardTasks() {
        return new ArrayList<>(tasks);
    }

    public void addTask(Task taskToAdd){
        tasks.add(taskToAdd);

    }

    @Override
    public String print() {
        return String.format("-%s %n", getName());

    }


    @Override
    public String printTasks() {

        if (tasks.size() == 0) {
            return String.format("#Board Tasks: %s%n" +
                    " #No tasks in this board", name);
        }
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("#Board Tasks: %s%n", name));
        for (Task task : tasks) {
            strBuilder.append(task.print());
            strBuilder.append(String.format(" --- %n"));
        }
        strBuilder.append(" ========== ");
        return strBuilder.toString();
    }
}
