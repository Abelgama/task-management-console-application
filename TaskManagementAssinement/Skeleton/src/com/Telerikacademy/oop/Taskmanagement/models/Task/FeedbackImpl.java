package com.Telerikacademy.oop.Taskmanagement.models.Task;

import com.Telerikacademy.oop.Taskmanagement.models.common.enums.*;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Feedback;

public class FeedbackImpl extends TaskImpl implements Feedback {

    private int rating;
    private FeedbackStatus status;

    public FeedbackImpl(String title, String description, FeedbackStatus status, int rating) {
        super(title, description);
        setRating(rating);
        setStatus(status);
    }
    public void setRating( int rating) {
        this.rating = rating;
    }

    public void setStatus(FeedbackStatus status) {
        this.status = status;
    }

    @Override
    public  void modifyRating(int rating){
        setRating(rating);
    }

    @Override
    public  void modifyStatus(FeedbackStatus status){
        setStatus(status);
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public FeedbackStatus getStatus() {
        return status;
    }
}
