package com.Telerikacademy.oop.Taskmanagement.models.contracts;

import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;

public interface Assignable extends Task {

    Priority getPriority();

    Member getAssignee();

    void modifyAssignee(Member assignee);
}
