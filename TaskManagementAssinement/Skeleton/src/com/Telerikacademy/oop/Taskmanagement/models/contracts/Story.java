package com.Telerikacademy.oop.Taskmanagement.models.contracts;

import com.Telerikacademy.oop.Taskmanagement.models.common.enums.BugStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Size;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.StoryStatus;

public interface Story extends Assignable {

    Size getSize();

    StoryStatus getStatus();

    void modifyPriority(Priority priority);

    void modifyStatus(StoryStatus status);

    void modifySize(Size size);
}
