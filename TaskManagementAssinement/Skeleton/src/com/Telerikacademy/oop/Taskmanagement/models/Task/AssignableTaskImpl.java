package com.Telerikacademy.oop.Taskmanagement.models.Task;

import com.Telerikacademy.oop.Taskmanagement.models.MembersImpl;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Assignable;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;

public abstract class AssignableTaskImpl extends TaskImpl implements Assignable {

    private Priority priority;
    private Member assignee;

    public AssignableTaskImpl(String title, String description, Priority priority) {
        super(title, description);
        setPriority(priority);
        setDefaultAssignee();
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void setDefaultAssignee() {
        Member defaultAssignee = new MembersImpl("Default Assignee");

        this.assignee = defaultAssignee;
    }

    public void setAssignee(Member assignee) {

        this.assignee = assignee;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Member getAssignee() {
        return assignee;
    }

    @Override
    public void modifyAssignee(Member assignee) {

    }
}
