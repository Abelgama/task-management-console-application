package com.Telerikacademy.oop.Taskmanagement.models.common;

public class Constants {

    public static final int TITLE_LEN_MIN = 10;
    public static final int TITLE_LEN_MAX = 50;
    public static final String TITLE_ERROR_MESSAGE ="Title must be between 10 and 50 characters" ;
    public static final String TITLE_NOT_NULL_MESSAGE ="Title can not be null" ;

    public static final int DESCRIPTION_LEN_MIN = 10;
    public static final int DESCRIPTION_LEN_MAX = 500;
    public static final String DESCRIPTION_ERROR_MESSAGE ="Description must be between 10 and 500 characters" ;
    public static final String DESCRIPTION_NOT_NULL_MESSAGE ="Description can not be null" ;

    public static final int CONTENT_LEN_MIN = 10;
    public static final int CONTENT_LEN_MAX = 200;
    public static final String CONTENT_ERROR_MESSAGE ="Content must be between 10 and 200 characters" ;
    public static final String CONTENT_NOT_NULL_MESSAGE ="Content can not be null" ;

    public static final String AUTHOR_NOT_NULL_MESSAGE ="Author can not be null" ;





}
