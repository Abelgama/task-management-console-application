package com.Telerikacademy.oop.Taskmanagement.models.contracts;

import com.Telerikacademy.oop.Taskmanagement.models.common.enums.BugStatus;

import java.util.List;

public interface Task extends Commentable {

    int getID();

    String getTitle();

    String getDescription();

    StatusBase getStatus();

    List<String> getHistory();

    String print();
}
