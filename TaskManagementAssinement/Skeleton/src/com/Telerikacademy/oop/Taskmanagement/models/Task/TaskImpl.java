package com.Telerikacademy.oop.Taskmanagement.models.Task;

import com.Telerikacademy.oop.Taskmanagement.Exceptions.InvalidUserInputException;
import com.Telerikacademy.oop.Taskmanagement.models.common.Validator;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Comment;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.StatusBase;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Task;

import java.util.ArrayList;
import java.util.List;

import static com.Telerikacademy.oop.Taskmanagement.models.common.Constants.*;

public abstract class TaskImpl implements Task {
 private static int uniqueID = 0;
 private  int taskID ;
 private String title;
 private String description;
 private List<Comment> comments;
 private List<String> history;


 public TaskImpl (String title, String description){
       setTaskID(taskID);
       setTitle(title);
       setDescription(description);
       comments = new ArrayList<>();
       history = new ArrayList<>();


 }

    public void setTaskID(int taskID) {

     this.taskID =++uniqueID ;


    }

    public void setTitle(String title) {
      Validator.validateArgumentIsNotNull(title,TITLE_NOT_NULL_MESSAGE);
      Validator.validateIntRange(title.length(),TITLE_LEN_MIN,TITLE_LEN_MAX,TITLE_ERROR_MESSAGE);
        this.title = title;
    }

    public void setDescription(String description) {
        Validator.validateArgumentIsNotNull(description,DESCRIPTION_NOT_NULL_MESSAGE);
        Validator.validateIntRange(description.length(),DESCRIPTION_LEN_MIN,DESCRIPTION_LEN_MAX,DESCRIPTION_ERROR_MESSAGE);
        this.description = description;
    }

    @Override
    public List<Comment> getComments() {
        return comments;
    }

    @Override
    public int getID() {
        return taskID ;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<String> getHistory() {
        return history;
    }

    @Override
    public String print() {
        return String.format(" Task ID: %d %n Title: %s%n", getID(),getTitle());

    }



}
