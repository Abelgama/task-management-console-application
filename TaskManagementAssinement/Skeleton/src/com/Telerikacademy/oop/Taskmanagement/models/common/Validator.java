package com.Telerikacademy.oop.Taskmanagement.models.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    public static void validateIntRange(int value, int min, int max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateDecimalRange(double value, double min, double max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }


    public static void validateArgumentIsNotNull(Object arg, String message) {
        if (arg == null) {
            throw new IllegalArgumentException(message);
        }
    }

}
