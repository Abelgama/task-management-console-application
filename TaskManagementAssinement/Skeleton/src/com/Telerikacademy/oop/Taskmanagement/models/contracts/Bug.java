package com.Telerikacademy.oop.Taskmanagement.models.contracts;

import com.Telerikacademy.oop.Taskmanagement.models.common.enums.BugStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.TaskSeverity;

public interface Bug extends Assignable {

    TaskSeverity getSeverity();

    BugStatus getStatus();

    void modifyPriority(Priority priority);

    void modifyStatus(BugStatus status);

    void modifySeverity(TaskSeverity severity);

}
