package com.Telerikacademy.oop.Taskmanagement.models.common.enums;

import com.Telerikacademy.oop.Taskmanagement.models.contracts.StatusBase;

public enum BugStatus implements StatusBase {
    ACTIVE,
    FIXED;

    @Override
    public String toString() {
        switch (this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            default:
                return "";
        }
    }
}
