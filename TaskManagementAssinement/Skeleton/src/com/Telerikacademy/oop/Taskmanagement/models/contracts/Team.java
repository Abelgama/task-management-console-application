package com.Telerikacademy.oop.Taskmanagement.models.contracts;

import java.util.List;

public interface Team  {

    String getName();

    List<Board> getTeamBoards();

    List<Member> getTeamMembers();

    void addMember(Member member);

    void addBoard (Board board);

    String printMembers();

    String printBoards();
}
