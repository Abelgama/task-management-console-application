package com.Telerikacademy.oop.Taskmanagement;

import com.Telerikacademy.oop.Taskmanagement.core.TaskManagementEngine;

public class Startup {

    public static void main(String[] args) {
        TaskManagementEngine engine = new TaskManagementEngine();
        engine.start();
    }
}
