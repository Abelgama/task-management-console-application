package com.Telerikacademy.oop.Taskmanagement.models;

import com.Telerikacademy.oop.Taskmanagement.Exceptions.InvalidUserInputException;
import com.Telerikacademy.oop.Taskmanagement.models.Task.FeedbackImpl;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.FeedbackStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.Telerikacademy.oop.Taskmanagement.models.common.Constants;

public class FeedbackImplTests {
    @Test
    public void constructor_Should_CreateFeedback_When_ArgumentsAreValid() {
        FeedbackImpl feedback = new FeedbackImpl("titletitle", "description", FeedbackStatus.NEW, 5);

        Assertions.assertEquals("titletitle", feedback.getTitle());
        Assertions.assertEquals("description", feedback.getDescription());
        Assertions.assertEquals(FeedbackStatus.NEW, feedback.getStatus());
        Assertions.assertEquals(5, feedback.getRating());

    }

    @Test
    public void constructor_Should_ThrowException_When_TitleIsInvalid() {
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new FeedbackImpl("f","description",FeedbackStatus.NEW,5));

    }
    @Test
    public void constructor_Should_ThrowException_When_DescriptionIsInvalid() {
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new FeedbackImpl("titletitle", "p", FeedbackStatus.NEW,10));
    }

    @Test
    public void constructor_Should_ThrowException_When_RatingIsChanged() {
        FeedbackImpl feedback = new FeedbackImpl("titletitle", "description", FeedbackStatus.NEW, 5);
        feedback.setRating(6);
        Assertions.assertEquals(6,feedback.getRating());

    }

    @Test
    public void constructor_Should_ThrowException_When_StatusIsChanged() {
        FeedbackImpl feedback = new FeedbackImpl("titletitle", "description", FeedbackStatus.NEW, 5);
        feedback.setStatus(FeedbackStatus.DONE);
        Assertions.assertEquals(FeedbackStatus.DONE,feedback.getStatus());

    }




}
