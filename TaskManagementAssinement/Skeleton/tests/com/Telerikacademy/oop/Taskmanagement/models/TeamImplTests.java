package com.Telerikacademy.oop.Taskmanagement.models;


import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class TeamImplTests {

    @Test
    public void constructor_Should_CreateTeam_When_ArgumentsAreValid() {
        TeamImpl team = new TeamImpl("name");

        assertEquals("name", team.getName());
    }
}
