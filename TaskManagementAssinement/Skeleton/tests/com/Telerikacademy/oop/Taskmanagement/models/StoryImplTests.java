package com.Telerikacademy.oop.Taskmanagement.models;

import com.Telerikacademy.oop.Taskmanagement.models.Task.FeedbackImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.StoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.FeedbackStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Size;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.StoryStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StoryImplTests {
    @Test
    public void constructor_Should_CreateFeedback_When_ArgumentsAreValid() {
        StoryImpl story = new StoryImpl("storytitle", "description", StoryStatus.DONE, Priority.HIGH, Size.LARGE);

        Assertions.assertEquals("storytitle", story.getTitle());
        Assertions.assertEquals("description", story.getDescription());
        Assertions.assertEquals(StoryStatus.DONE, story.getStatus());
        Assertions.assertEquals(Priority.HIGH,story.getPriority());
        Assertions.assertEquals(Size.LARGE,story.getSize());
    }

}
