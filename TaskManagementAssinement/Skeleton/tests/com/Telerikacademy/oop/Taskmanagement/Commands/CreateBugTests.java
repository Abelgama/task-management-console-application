package com.Telerikacademy.oop.Taskmanagement.Commands;

import com.Telerikacademy.oop.Taskmanagement.commands.CreateBug;
import com.Telerikacademy.oop.Taskmanagement.commands.ShowAllTeamMembers;
import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.TaskManagementRepositoryImpl;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.BoardImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.BugImpl;
import com.Telerikacademy.oop.Taskmanagement.models.TeamImpl;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.BugStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.TaskSeverity;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBugTests {
    private TaskManagementFactory factory;
    private TaskManagementRepository repository;
    private CreateBug command;
    private List<String> parameters;

    @BeforeEach
    public void setupTest() {
        repository = new TaskManagementRepositoryImpl();
        factory = new TaskManagementFactoryImpl();
        command = new CreateBug(factory,repository);
        parameters = new ArrayList<>();

    }


    @Test
    public void execute_Should_AddNewBugToRepository_When_ValidParameters() {
        Board board = new BoardImpl("board");
        repository.getBoards().put("board",board);
        parameters.add("board");
        parameters.add("titleOfBug");
        parameters.add("description");
        parameters.add(String.valueOf(BugStatus.ACTIVE));
        parameters.add(String.valueOf(Priority.HIGH));
        parameters.add(String.valueOf(TaskSeverity.CRITICAL));

        command.execute(parameters);


        Assertions.assertTrue(repository.getBugs().containsKey(1));
    }


       // Assertions.assertTrue(board.getBoardTasks().contains(repository.getBugs().get(1)));

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(
                IndexOutOfBoundsException.class,
                () -> command.execute(parameters));
    }


    @Test
    public void execute_Should_ThrowException_When_BoardNotExist() {
        repository.getBoards().put("name", new BoardImpl("name"));
        parameters.add("notExist");
        parameters.add("title");
        parameters.add("description");
        parameters.add(String.valueOf(BugStatus.ACTIVE));
        parameters.add(String.valueOf(Priority.HIGH));
        parameters.add(String.valueOf(TaskSeverity.CRITICAL));

        Assertions.assertEquals(String.format(CommandConstants.BOARD_DOES_NOT_EXIST, "notExist"), command.execute(parameters));
    }
}
