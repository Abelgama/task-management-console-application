package com.Telerikacademy.oop.Taskmanagement.Commands;

import com.Telerikacademy.oop.Taskmanagement.commands.ShowTasksOfMember;
import com.Telerikacademy.oop.Taskmanagement.commands.ShowTeamBoards;
import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.TaskManagementRepositoryImpl;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.BoardImpl;
import com.Telerikacademy.oop.Taskmanagement.models.MembersImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.BugImpl;
import com.Telerikacademy.oop.Taskmanagement.models.TeamImpl;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.BugStatus;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.Priority;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.TaskSeverity;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Member;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowTeamBoardsTests {
    private TaskManagementFactory factory;
    private TaskManagementRepository repository;
    private ShowTeamBoards command;
    private List<String> parameters;

    @BeforeEach
    public void setupTest() {
        repository = new TaskManagementRepositoryImpl();
        factory = new TaskManagementFactoryImpl();
        command = new ShowTeamBoards(factory,repository);
        parameters = new ArrayList<>();
    }

    @Test
    public void execute_Should_FinishWithoutException_When_ValidParameters() {

        Team team = new TeamImpl("team");
        repository.getTeams().put("team",team);
        Board board = new BoardImpl("board");
        parameters.add("team");

        team.addBoard(board);
        command.execute(parameters);

        Assertions.assertEquals(team.printBoards(), command.getResult());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(
                IndexOutOfBoundsException.class,
                () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_TeamNotExist() {
        repository.getTeams().put("team",new TeamImpl("team"));
        parameters.add("notExist");

        Assertions.assertEquals(String.format(CommandConstants.TEAM_DOES_NOT_EXIST, "notExist"), command.execute(parameters));
    }
}
