package com.Telerikacademy.oop.Taskmanagement.Commands;

import com.Telerikacademy.oop.Taskmanagement.commands.CreateStory;
import com.Telerikacademy.oop.Taskmanagement.commands.CreateTeam;
import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.TaskManagementRepositoryImpl;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.TeamImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateTeamTests {
    private TaskManagementFactory factory;
    private TaskManagementRepository repository;
    private CreateTeam command;
    private List<String> parameters;

    @BeforeEach
    public void setupTest() {
        repository = new TaskManagementRepositoryImpl();
        factory = new TaskManagementFactoryImpl();
        command = new CreateTeam(factory,repository);
        parameters = new ArrayList<>();

    }

    @Test
    public void execute_Should_AddNewTeamToRepository_When_ValidParameters() {
        parameters.add("name");

        command.execute(parameters);

        Assertions.assertTrue(repository.getTeams().containsKey("name"));
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(
                IndexOutOfBoundsException.class,
                () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateCategoryName() {
        repository.getTeams().put("name", new TeamImpl("name"));
        parameters.add("name");

        Assertions.assertEquals(String.format(CommandConstants.TEAM_EXISTS, "name"), command.execute(parameters));

    }
}
