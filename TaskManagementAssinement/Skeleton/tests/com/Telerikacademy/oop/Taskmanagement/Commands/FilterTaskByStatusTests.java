package com.Telerikacademy.oop.Taskmanagement.Commands;

import com.Telerikacademy.oop.Taskmanagement.commands.FilterTasksByStatus;
import com.Telerikacademy.oop.Taskmanagement.commands.ShowAllTasks;
import com.Telerikacademy.oop.Taskmanagement.core.TaskManagementRepositoryImpl;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.BugImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.FeedbackImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.StoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.*;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Feedback;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Story;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterTaskByStatusTests {

    private TaskManagementFactory factory;
    private TaskManagementRepository repository;
    private FilterTasksByStatus command;
    private List<String> parameters;


    @BeforeEach
    public void setupTest() {
        repository = new TaskManagementRepositoryImpl();
        factory = new TaskManagementFactoryImpl();
        command = new FilterTasksByStatus(factory,repository);
        parameters = new ArrayList<>();



    }

    @Test
    public void execute_Should_FilterAllTasks_When_ValidParameters() {
        Bug bug = new BugImpl("titleOfBug","description", BugStatus.ACTIVE, Priority.HIGH, TaskSeverity.CRITICAL);
        Story story1 = new StoryImpl("titleOfStory1","description", StoryStatus.DONE, Priority.HIGH, Size.LARGE);
        Story story2 = new StoryImpl("titleOfStory2","description", StoryStatus.NOTDONE, Priority.HIGH, Size.LARGE);
        Story story3 = new StoryImpl("titleOfStory3","description", StoryStatus.NOTDONE, Priority.HIGH, Size.LARGE);
        Feedback feedback = new FeedbackImpl("titleOfFeedback", "description",FeedbackStatus.NEW,5) ;
        repository.getTasks().put(bug.getID(),bug);
        repository.getTasks().put(story1.getID(),story1);
        repository.getTasks().put(story2.getID(),story2);
        repository.getTasks().put(story3.getID(),story3);
        repository.getTasks().put(feedback.getID(),feedback);
        parameters.add("notdone");

        Assertions.assertEquals(String.format("****FILTERED BY STATUS****%n%d. Task ID: %d%n Title: %s%n%d. Task ID: %d%n Title: %s%n"
                ,1,story2.getID(),story2.getTitle(),2,story3.getID(),story3.getTitle()), command.execute(parameters));
    }


    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(
                IndexOutOfBoundsException.class,
                () -> command.execute(parameters));
    }

}
