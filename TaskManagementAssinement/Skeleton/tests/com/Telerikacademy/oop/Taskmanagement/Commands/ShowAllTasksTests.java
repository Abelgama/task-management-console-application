package com.Telerikacademy.oop.Taskmanagement.Commands;

import com.Telerikacademy.oop.Taskmanagement.commands.ShowAllTasks;
import com.Telerikacademy.oop.Taskmanagement.commands.ShowTeamBoards;
import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.TaskManagementRepositoryImpl;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.BoardImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.BugImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.FeedbackImpl;
import com.Telerikacademy.oop.Taskmanagement.models.Task.StoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.TeamImpl;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.*;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllTasksTests {
    private TaskManagementFactory factory;
    private TaskManagementRepository repository;
    private ShowAllTasks command;
    private List<String> parameters;


    @BeforeEach
    public void setupTest() {
        repository = new TaskManagementRepositoryImpl();
        factory = new TaskManagementFactoryImpl();
        command = new ShowAllTasks(factory,repository);
        parameters = new ArrayList<>();



    }

    @Test
    public void execute_Should_ShowAllTasks_When_ValidParameters() {
        Bug bug = new BugImpl("titleOfBug","description", BugStatus.ACTIVE, Priority.HIGH, TaskSeverity.CRITICAL);
        Story story = new StoryImpl("titleOfStory","description", StoryStatus.NOTDONE, Priority.HIGH, Size.LARGE);
        Feedback feedback = new FeedbackImpl("titleOfFeedback", "description",FeedbackStatus.NEW,5) ;
        repository.getBugs().put(bug.getID(),bug);
        repository.getStories().put(story.getID(),story);
        repository.getFeedbacks().put(feedback.getID(),feedback);
        parameters.add("all");


        command.execute(parameters);

        Assertions.assertEquals(String.format("****TASKS****%n%d. Task ID: %d%n Title: %s%n" +
                "%d. Task ID: %d%n Title: %s%n" +
                "%d. Task ID: %d%n Title: %s%n",1,bug.getID(),bug.getTitle(),2,story.getID(),story.getTitle(),
                3,feedback.getID(),feedback.getTitle()), command.getResult());
    }

    @Test
    public void execute_Should_ShowAllBugs_When_ValidParameters() {
        Bug bug = new BugImpl("titleOfBug","description", BugStatus.ACTIVE, Priority.HIGH, TaskSeverity.CRITICAL);
        Story story = new StoryImpl("titleOfStory","description", StoryStatus.NOTDONE, Priority.HIGH, Size.LARGE);
        Feedback feedback = new FeedbackImpl("titleOfFeedback", "description",FeedbackStatus.NEW,5) ;
        repository.getBugs().put(bug.getID(),bug);
        repository.getStories().put(story.getID(),story);
        repository.getFeedbacks().put(feedback.getID(),feedback);
        parameters.add("bug");

        command.execute(parameters);

        Assertions.assertEquals(String.format("****BUGS****%n%d. Task ID: %d%n Title: %s%n",1,bug.getID(),bug.getTitle()), command.getResult());
    }

    @Test
    public void execute_Should_ShowAllStories_When_ValidParameters() {
        Bug bug = new BugImpl("titleOfBug","description", BugStatus.ACTIVE, Priority.HIGH, TaskSeverity.CRITICAL);
        Story story = new StoryImpl("titleOfStory","description", StoryStatus.NOTDONE, Priority.HIGH, Size.LARGE);
        Feedback feedback = new FeedbackImpl("titleOfFeedback", "description",FeedbackStatus.NEW,5) ;
        repository.getBugs().put(bug.getID(),bug);
        repository.getStories().put(story.getID(),story);
        repository.getFeedbacks().put(feedback.getID(),feedback);
        parameters.add("story");

        command.execute(parameters);

        Assertions.assertEquals(String.format("****STORIES****%n%d. Task ID: %d%n Title: %s%n",1,story.getID(),story.getTitle()), command.getResult());
    }

    @Test
    public void execute_Should_ShowAllFeedbacks_When_ValidParameters() {
        Bug bug = new BugImpl("titleOfBug","description", BugStatus.ACTIVE, Priority.HIGH, TaskSeverity.CRITICAL);
        Story story = new StoryImpl("titleOfStory","description", StoryStatus.NOTDONE, Priority.HIGH, Size.LARGE);
        Feedback feedback = new FeedbackImpl("titleOfFeedback", "description",FeedbackStatus.NEW,5) ;
        repository.getBugs().put(bug.getID(),bug);
        repository.getStories().put(story.getID(),story);
        repository.getFeedbacks().put(feedback.getID(),feedback);
        parameters.add("feedback");

        command.execute(parameters);

        Assertions.assertEquals(String.format("****FEEDBACKS****%n%d. Task ID: %d%n Title: %s%n",1,feedback.getID(),feedback.getTitle()), command.getResult());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(
                IndexOutOfBoundsException.class,
                () -> command.execute(parameters));
    }

}
