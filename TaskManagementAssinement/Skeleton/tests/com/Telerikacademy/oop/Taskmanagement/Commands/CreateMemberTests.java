package com.Telerikacademy.oop.Taskmanagement.Commands;

import com.Telerikacademy.oop.Taskmanagement.commands.CreateMember;
import com.Telerikacademy.oop.Taskmanagement.commands.CreateTeam;
import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.TaskManagementRepositoryImpl;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.MembersImpl;
import com.Telerikacademy.oop.Taskmanagement.models.TeamImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateMemberTests {
    private TaskManagementFactory factory;
    private TaskManagementRepository repository;
    private CreateMember command;
    private List<String> parameters;

    @BeforeEach
    public void setupTest() {
        repository = new TaskManagementRepositoryImpl();
        factory = new TaskManagementFactoryImpl();
        command = new CreateMember(factory,repository);
        parameters = new ArrayList<>();

    }

    @Test
    public void execute_Should_AddNewTeamToRepository_When_ValidParameters() {
        parameters.add("name");

        command.execute(parameters);

        Assertions.assertTrue(repository.getMembers().containsKey("name"));
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(
                IndexOutOfBoundsException.class,
                () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateCategoryName() {
        repository.getMembers().put("name", new MembersImpl("name"));
        parameters.add("name");

        Assertions.assertEquals(String.format(CommandConstants.MEMBER_EXISTS, "name"), command.execute(parameters));

    }
}
