package com.Telerikacademy.oop.Taskmanagement.Commands;

import com.Telerikacademy.oop.Taskmanagement.commands.CreateBug;
import com.Telerikacademy.oop.Taskmanagement.commands.CreateStory;
import com.Telerikacademy.oop.Taskmanagement.commands.constants.CommandConstants;
import com.Telerikacademy.oop.Taskmanagement.core.TaskManagementRepositoryImpl;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementFactory;
import com.Telerikacademy.oop.Taskmanagement.core.contracts.TaskManagementRepository;
import com.Telerikacademy.oop.Taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.Telerikacademy.oop.Taskmanagement.models.BoardImpl;
import com.Telerikacademy.oop.Taskmanagement.models.common.enums.*;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Board;
import com.Telerikacademy.oop.Taskmanagement.models.contracts.Bug;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateStoryTests {
    private TaskManagementFactory factory;
    private TaskManagementRepository repository;
    private CreateStory command;
    private List<String> parameters;

    @BeforeEach
    public void setupTest() {
        repository = new TaskManagementRepositoryImpl();
        factory = new TaskManagementFactoryImpl();
        command = new CreateStory(factory,repository);
        parameters = new ArrayList<>();

    }


    @Test
    public void execute_Should_AddNewStoryToRepository_When_ValidParameters() {
        Board board = new BoardImpl("board");
        repository.getBoards().put("board",board);
        parameters.add("board");
        parameters.add("titleOfStory");
        parameters.add("description");
        parameters.add(String.valueOf(StoryStatus.NOTDONE));
        parameters.add(String.valueOf(Priority.HIGH));
        parameters.add(String.valueOf(Size.SMALL));

        command.execute(parameters);


        Assertions.assertTrue(repository.getStories().size() == 1);
    }


    // Assertions.assertTrue(board.getBoardTasks().contains(repository.getBugs().get(1)));



    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(
                IndexOutOfBoundsException.class,
                () -> command.execute(parameters));
    }


    @Test
    public void execute_Should_ThrowException_When_BoardNotExist() {
        repository.getBoards().put("name", new BoardImpl("name"));
        parameters.add("notExist");
        parameters.add("titleOfStory");
        parameters.add("description");
        parameters.add(String.valueOf(StoryStatus.DONE));
        parameters.add(String.valueOf(Priority.HIGH));
        parameters.add(String.valueOf(Size.SMALL));

        Assertions.assertEquals(String.format(CommandConstants.BOARD_DOES_NOT_EXIST, "notExist"), command.execute(parameters));
    }
}
